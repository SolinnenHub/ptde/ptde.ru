function get(name) {
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

if (products.length == 0) {
	$("#market-items").append("<p>Товаров пока нет!</p>");
} else {
	var items = "";
	products.forEach(product => {
		items += '<div id="'+product['_id']+'" class="item"><div class="preview"><p class="title">'+product['name']+', '+product['price']+'р.</p><div class="image"><div class="img" style="background-image: url(../images/donate/icons/'+product['icon']+');"></div></div></div><div class="buy-area"><div class="btn">В корзину</div><input class="amount" type="number" value="1" min=1 max=99></div></div>';
	});
	$("#market-items").append(items);
}
