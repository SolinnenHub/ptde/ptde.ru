const accesslog = require('access-log');
const vhost = require('vhost')
const express = require('express');
const nunjucks = require('nunjucks');
const path = require("path");
const app = express();

const config = require('./config');

const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
	name: String,
	desc: String,
	price: Number,
	icon: String,
});

var products = [];

function sleep(s) {
	return new Promise(resolve => setTimeout(resolve, s*1000));
}

async function main() {
	while (1) {
		await mongoose.connect(config.mongodb_uri);

		const Product = mongoose.model('Products', productSchema);
		
		/*const book = new Product({
			'name': 'Фолиант Великой Топи',
			'desc': '*описание*',
			price: 700,
			'icon': 'book.png'
		});
		await book.save();*/
		
		products = await Product.find();
		console.log(products.length+" products have been loaded.");

		await mongoose.disconnect();
		await sleep(60*30);
	}
}

main().catch(err => console.log(err));


nunjucks.configure('templates', {
	autoescape: true,
	express: app
});

app.use(express.static(path.join(__dirname, 'static')));


app.use(vhost('mc.*', function (req, res) {
	accesslog(req, res);
	res.render('howtojoin.html');
}))


app.get('/', function(req, res) {
	accesslog(req, res);
	res.render('index.html');
});

app.get('/donate', function(req, res) {
	accesslog(req, res);
	res.render('donate.html', {"products": products});
});

app.get('/info', function(req, res) {
	accesslog(req, res);
	res.render('info.html');
});

app.get('/contact', function(req, res) {
	accesslog(req, res);
	res.render('contact.html');
});

app.get('*', function(req, res) {
	accesslog(req, res);
	res.status(404).send('Error 404');
});

if (!module.parent) {
	var port = 3000;
	var args = process.argv.slice(2);
	switch (args[0]) {
		case '--port':
			port = parseInt(args[1]);
			break;
	}
	app.listen(port);
	console.log('Express started on port '+port);
}