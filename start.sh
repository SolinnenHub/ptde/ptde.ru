#!/bin/bash

ARGS=("$@")
SESSION='ptderu'

if ! [[ $(tmux ls | grep $SESSION) ]]; then
	tmux new-session -d -s $SESSION
	tmux send-keys 'npm start' C-m
fi

# если выполнить с аргументом -d, то подключение к сессии не произойдет
if ! [[ $ARGS = "-d" || $ARGS = "--daemon" ]]; then
	tmux attach-session -t $SESSION
fi